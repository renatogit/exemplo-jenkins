package tech.mastertech.itau.produto.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.produto.models.Fornecedor;
import tech.mastertech.itau.produto.models.Pedido;
import tech.mastertech.itau.produto.models.Produto;
import tech.mastertech.itau.produto.repositories.PedidoRepository;

//"Sincroniza" o JUnit com o ambiente de testes do spring
@RunWith(SpringRunner.class)

/*
 * Afim de tornar a execução do teste mais performatica, iremos pedir para que o spring
 * carregue somente algumas classes ao contexto, ao invez de todas as classes do projeto (que é o 
 * comportamento padrão do spring).
 * 
 * Para fazer isso iremos usar a anotação @ContextConfiguration e iremos passar quais classes
 * queremos carregadas, no nosso caso só precisamos da PedidoService
 */
@ContextConfiguration(classes = PedidoService.class)
public class PedidoServiceTest {
	
  //Carrega a instancia global do PedidoService
  @Autowired
  private PedidoService sujeito;
  
  /*
   * Como estamos executando um teste unitário e não um teste de integração,
   * não queremos que nosso repositório tenha seu comportamento padrão que é 
   * a integração com o banco de dados.
   * 
   * Para isso vamos "Mockar" o repositório, isso vai nos permitir mais pra frente
   * simular os comportamentos do repository da forma que precisarmos.
   * 
   * Para "Mockar" um repositório simplesmente colocamos a anotação @MockBean a váriavel da classe.
   * */
  @MockBean
  private PedidoRepository pedidoRepository;
  
  //@Test indica ao JUnit que esse metodo deve ser testado
  @Test
  public void deveSalvarUmaCategoria() {
	//aqui criamos o pedido que iremos usar de teste do salvamento
    Pedido pedido = new Pedido();
    pedido.setFornecedor(new Fornecedor());
    pedido.setProdutos(Lists.list(new Produto()));
    
    /*
     * Conforme falamos a cima, iremos definir um comportamento simulado para nosso repository,
     * ou seja, um retorno que gostariamos que ele tivesse.
     * 
     * Para isso, usamos o formato:
     *  - when(<uso do repository a ser simulado>).thenReturn(<retorno que queremos simular>)
     *  
     * No nosso caso temos:
     *  - when(pedidoRepository.save(pedido)).thenReturn(pedido);
     * Ou seja, quando a função save(pedido) for chamada do nosso repositorio,
     * iremos fazer com que isso retorne nosso pedido
     * */
    when(pedidoRepository.save(pedido)).thenReturn(pedido);    
    
    /*
     * Aqui vamos executar a ação de criar um pedido usando o setPedido.
     * 
     * Note que, o sujeito.setPedido executa dentro dele o pedidoRepository.save que seria um problema para nós
     * mas como já mockamos o repository e estamos simulando esse comportamento, não teremos nenhum problema,
     * conseguiremos testar tranquilamente todo o comportamento do service
     * */
    Pedido pedidoSalvo = sujeito.setPedido(pedido);
    
    /*
     * Por fim, agora que temos o pedido que criamos, e o pedido que o service salvou,
     * podemos verificar se os dois objetos coincidem nas propiedades que definimos
     * assim estamos verificando se o service realmente cumpriu sua função de salvar um produto
     * 
     * para verificar se duas coisas são iguais no JUnit usamos o assertEquals(Objeto1, Objeto2)
     * */
    assertEquals(pedido.getFornecedor(), pedidoSalvo.getFornecedor());
    assertEquals(pedido.getProdutos(), pedidoSalvo.getProdutos());
    assertEquals(LocalDate.now(), pedido.getData());
  }
}
